using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProxyDesignPattern;

namespace ProxyUnitTest
{
    [TestClass]
    public class ProxyTest
    {

        [TestMethod]
        public void TestProxyDisplayImage()
        {

            // Arrange
            var AJImage = new ProxyImage("Anne Jan.png");

            // Act
            var act = AJImage.DisplayImage();

            // Assert
            Assert.AreEqual("displayed", act);
        }

        [TestMethod]
        public void TestProxyImageNotLoaded()
        {

            // Arrange
            var AJImage = new ProxyImage("Anne Jan.png");

            // Assert
            Assert.IsFalse(AJImage.IsLoaded);
        }

        [TestMethod]
        public void TestProxyImageIsLoaded()
        {

            // Arrange
            var AJImage = new ProxyImage("Anne Jan.png");

            // Act
            AJImage.DisplayImage();

            // Assert
            Assert.IsTrue(AJImage.IsLoaded);
        }
    }
}
