﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyDesignPattern
{
    public interface IImage
    {
        string DisplayImage();

    }

}
