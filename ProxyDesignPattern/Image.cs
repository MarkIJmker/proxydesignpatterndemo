﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyDesignPattern
{
    public class Image : IImage
    {
        private string Filename { get; set; }
        
        public Image(string filename)
        {
            Filename = filename;
            LoadImageFromFloppyDisk();
        }

        public void LoadImageFromFloppyDisk()
        {
            Console.WriteLine("Loading Image : " + Filename);
        }

        public string DisplayImage()
        {
            Console.WriteLine("Displaying Image : " + Filename);
            return "displayed";
        }

    }

}
