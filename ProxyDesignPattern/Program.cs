﻿using System;

namespace ProxyDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var AJImage = new Image("Anne Jan.png");

            Console.WriteLine("Anne Jan #1:");
            AJImage.DisplayImage(); // Needs to be load

            Console.WriteLine("Anne Jan #2:");
            AJImage.DisplayImage(); // Already loaded

            Console.WriteLine("Anne Jan #3:");
            AJImage.DisplayImage(); // Already loaded

            Console.WriteLine();


            var MarkImage = new ProxyImage("Mark.png");

            Console.WriteLine("Mark #1:");
            MarkImage.DisplayImage(); // Needs to load

            Console.WriteLine("Mark #2:");
            MarkImage.DisplayImage(); // Already loaded

            Console.ReadKey();
        }
    }
}
