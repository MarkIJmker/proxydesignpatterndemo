﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyDesignPattern
{
    public class ProxyImage : IImage
    {

        private Image realImage = null;
        public bool IsLoaded = false;

        private string Filename { get; set; }

        public ProxyImage(string filename)
        {
            Filename = filename;
        }

        public string DisplayImage()
        {
            if (realImage == null)
            {
                realImage = new Image(Filename);
                IsLoaded = true;
            }
            return realImage.DisplayImage();
        }

        
    }

}
